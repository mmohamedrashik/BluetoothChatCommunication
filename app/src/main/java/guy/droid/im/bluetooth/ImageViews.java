package guy.droid.im.bluetooth;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;

public class ImageViews extends AppCompatActivity {
ImageView image;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_views);
        image = (ImageView)findViewById(R.id.detail_image);
        Bundle extras = getIntent().getExtras();

        try
        {
            String path = extras.getString("file_path");
            File imgFile = new File(path);
            if(imgFile.exists()){

                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                image.setImageBitmap(myBitmap);

            }
        }catch (Exception e)
        {
             Toast.makeText(this, ""+e.toString(), Toast.LENGTH_SHORT).show();
        }
    }
}
