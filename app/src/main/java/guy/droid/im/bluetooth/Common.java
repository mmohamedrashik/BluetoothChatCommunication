package guy.droid.im.bluetooth;

import android.content.Context;
import android.graphics.Color;
import android.widget.Toast;

import com.muddzdev.styleabletoastlibrary.StyleableToast;

/**
 * Created by admin on 6/30/2017.
 */

public class Common {
    Common()
    {

    }
    public void Toasted(Context context, String message,String type)
    {
        if(type.equals(Constants.WARN))
        {
            StyleableToast  st = new StyleableToast
                    .Builder(context)
                    .text(message)
                    .textBold()
                    .textColor(Color.RED)
                    .duration(Toast.LENGTH_LONG)
                    .cornerRadius(3)
                    .backgroundColor(Color.BLACK)
                    .build();
            st.show();
        }
        if(type.equals(Constants.INFO))
        {
            StyleableToast  st = new StyleableToast
                    .Builder(context)
                    .text(message)
                    .textBold()
                    .textColor(Color.WHITE)
                    .duration(Toast.LENGTH_SHORT)
                    .cornerRadius(3)
                    .backgroundColor(Color.parseColor("#FFA726"))
                    .build();
            st.show();
        }

        if(type.equals(Constants.SUCCESS))
        {
            StyleableToast  st = new StyleableToast
                    .Builder(context)
                    .text(message)
                    .textBold()
                    .textColor(Color.WHITE)
                    .duration(Toast.LENGTH_SHORT)
                    .cornerRadius(3)
                    .backgroundColor(Color.parseColor("#00C853"))
                    .build();
            st.show();
        }
        if(type.equals(Constants.ERROR))
        {
            StyleableToast  st = new StyleableToast
                    .Builder(context)
                    .text(message)
                    .textBold()
                    .textColor(Color.WHITE)
                    .duration(Toast.LENGTH_SHORT)
                    .cornerRadius(3)
                    .backgroundColor(Color.RED)
                    .build();
            st.show();
        }
    }
}
