package guy.droid.im.bluetooth;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by admin on 7/1/2017.
 */

public class ChatRecycler  extends RecyclerView.Adapter<ChatRecycler.ViewHolder> {
    public static final String[] VALUES = new String[] {".jpg",".JPG",".png",".PNG"};
    MainActivity activity;
  //  int count;
    ChatContainer chatContainer;
    public ChatRecycler(MainActivity activity,ChatContainer chatContainer) {
        this.activity = activity;
        //this.count = count;
       this.chatContainer = chatContainer;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_layout,parent,false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

      //  holder.textView.setText(names.get(position));
        holder.sender_message_holder.setVisibility(View.GONE);
        holder.sender_image_holder.setVisibility(View.GONE);
        holder.sender_file_holder.setVisibility(View.GONE);
        holder.reciever_message_holder.setVisibility(View.GONE);
        holder.reciever_image_holder.setVisibility(View.GONE);
        holder.reciever_file_holder.setVisibility(View.GONE);




        if(chatContainer.getMessage_type().get(position) == Constants.M_RECIEVER)
        {
            holder.reciever_message_holder.setVisibility(View.VISIBLE);
            holder.reciever_message.setText(chatContainer.getReceiver_message().get(position));

        }
        if(chatContainer.getMessage_type().get(position) == Constants.M_SENDER)
        {
            holder.sender_message_holder.setVisibility(View.VISIBLE);
            holder.sender_message.setText(chatContainer.getSender_message().get(position));
        }
        if(chatContainer.getMessage_type().get(position) == Constants.M_SENDER_FILE && Arrays.asList(VALUES).contains(chatContainer.getRe_file_type().get(position)) == true)
        {
            holder.sender_image_holder.setVisibility(View.VISIBLE);
            Bitmap bitmap = BitmapDecoder(chatContainer.getRe_file().get(position));
            bitmap = getResizedBitmap(bitmap,1000);
            holder.sender_image.setImageBitmap(bitmap);
         //   holder.sender_message.setText(chatContainer.getSender_message().get(position));
        }
        if(chatContainer.getMessage_type().get(position) == Constants.M_SENDER_FILE && Arrays.asList(VALUES).contains(chatContainer.getRe_file_type().get(position)) != true)
        {
            holder.sender_file_holder.setVisibility(View.VISIBLE);
            Log.d("FILE_TYPE",chatContainer.getRe_file_type().get(position));
            if(chatContainer.getRe_file_type().get(position).equals(".pdf") || chatContainer.getRe_file_type().get(position).equals(".PDF"))
            {
                holder.sender_file.setImageResource(R.drawable.pdf);
            }else
            {
                holder.sender_file.setImageResource(R.drawable.copy);
            }


        }
        if(chatContainer.getMessage_type().get(position) == Constants.M_RECIEVER_FILE && Arrays.asList(VALUES).contains(chatContainer.getRe_file_type().get(position)) == true)
        {
            holder.reciever_image_holder.setVisibility(View.VISIBLE);
            Bitmap bitmap = BitmapDecoder(chatContainer.getRe_file().get(position));
            bitmap = getResizedBitmap(bitmap,1000);
            holder.reciever_image.setImageBitmap(bitmap);
            //   holder.sender_message.setText(chatContainer.getSender_message().get(position));
        }
        if(chatContainer.getMessage_type().get(position) == Constants.M_RECIEVER_FILE && Arrays.asList(VALUES).contains(chatContainer.getRe_file_type().get(position)) != true)
        {
            holder.reciever_file_holder.setVisibility(View.VISIBLE);
            if(chatContainer.getRe_file_type().get(position).equals(".pdf") || chatContainer.getRe_file_type().get(position).equals(".PDF"))
            {
                holder.reciever_file.setImageResource(R.drawable.pdf);
            }else
            {
                holder.reciever_file.setImageResource(R.drawable.copy);
            }


        }
        holder.sender_file_holder.setOnClickListener(Open(position,holder));
        holder.sender_image_holder.setOnClickListener(ImageOpen(position,holder));
        holder.reciever_file_holder.setOnClickListener(Open(position,holder));
        holder.reciever_image_holder.setOnClickListener(ImageOpen(position,holder));
    }
    public View.OnClickListener Open(final int position, final ViewHolder holder)
    {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // showEditDialog(position, holder);
                activity.FileOpen(position);
                //  holder.cardView.setBackgroundColor(Color.GREEN);

            }
        };
    }
    public View.OnClickListener ImageOpen(final int position, final ViewHolder holder)
    {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // showEditDialog(position, holder);
                activity.ImageOpen(position);
                //  holder.cardView.setBackgroundColor(Color.GREEN);

            }
        };
    }
    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }
    public Bitmap BitmapDecoder(File file)
    {
        if(file.exists()){

            Bitmap myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());


            return myBitmap;
        }else
        {
            return  null;
        }
    }
    @Override
    public int getItemCount() {
        return chatContainer.getMessage_type().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
       // TextView textView,textView2;
        RelativeLayout sender_message_holder,
               sender_image_holder,
               sender_file_holder,
               reciever_message_holder,
               reciever_image_holder,
               reciever_file_holder;
        TextView sender_message,reciever_message;

        ImageView sender_image,
                sender_file,
                reciever_image,
                reciever_file;


        public ViewHolder(View itemView) {
            super(itemView);

            sender_message_holder = (RelativeLayout)itemView.findViewById(R.id.sender_message_holder);
            sender_image_holder = (RelativeLayout)itemView.findViewById(R.id.sender_image_holder);
            sender_file_holder = (RelativeLayout)itemView.findViewById(R.id.sender_file_holder);
            reciever_message_holder = (RelativeLayout)itemView.findViewById(R.id.reciever_message_holder);
            reciever_image_holder = (RelativeLayout)itemView.findViewById(R.id.reciever_image_holder);
            reciever_file_holder = (RelativeLayout)itemView.findViewById(R.id.reciever_file_holder);

            sender_message = (TextView)itemView.findViewById(R.id.sender_message);
            reciever_message = (TextView)itemView.findViewById(R.id.reciever_message);

            sender_image = (ImageView)itemView.findViewById(R.id.sender_image);
            sender_file = (ImageView)itemView.findViewById(R.id.sender_file);
            reciever_image = (ImageView)itemView.findViewById(R.id.reciever_image);
            reciever_file = (ImageView)itemView.findViewById(R.id.reciever_file);



        }
    }
}
