package guy.droid.im.bluetooth;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by admin on 7/1/2017.
 */

public class ChatContainer {

    public ArrayList<String> getSender_message() {
        return sender_message;
    }

    public void setSender_message(ArrayList<String> sender_message) {
        this.sender_message = sender_message;
    }

    public ArrayList<String> getReceiver_message() {
        return receiver_message;
    }

    public void setReceiver_message(ArrayList<String> receiver_message) {
        this.receiver_message = receiver_message;
    }

    public ArrayList<String> getMessage_type() {
        return message_type;
    }

    public void setMessage_type(ArrayList<String> message_type) {
        this.message_type = message_type;
    }

    ArrayList<String> sender_message;
    ArrayList<String> receiver_message;
    ArrayList<String> message_type;

    public ArrayList<File> getRe_file() {
        return re_file;
    }

    public void setRe_file(ArrayList<File> re_file) {
        this.re_file = re_file;
    }

    public ArrayList<String> getRe_file_type() {
        return re_file_type;
    }

    public void setRe_file_type(ArrayList<String> re_file_type) {
        this.re_file_type = re_file_type;
    }

    ArrayList<File>    re_file;
    ArrayList<String>  re_file_type;
}
