package guy.droid.im.bluetooth;

import android.app.Activity;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.inputmethodservice.Keyboard;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import app.akexorcist.bluetotohspp.library.BluetoothSPP;
import app.akexorcist.bluetotohspp.library.BluetoothState;
import app.akexorcist.bluetotohspp.library.DeviceList;

public class MainActivity extends AppCompatActivity implements BluetoothSPP.OnDataReceivedListener,BluetoothSPP.BluetoothConnectionListener,BluetoothSPP.BluetoothStateListener{
    BluetoothSPP bt;
    EditText message;
    TextView message_history,connect_text;

    private int PICK_FILE_REQUEST = 1001;
    File file;
    byte[] bytes;
    String fileName;
    String extension;
    ProgressDialog pd;
    int BLUE_TOOTH_STATE = 0;
    private Toolbar toolbar;
    RelativeLayout chat_relative,home_layout;
    Animation fromtop,totop;

    RecyclerView chat_recycler;
    ChatRecycler chatRecycler;
    ChatContainer chatContainer;

    ArrayList<String> sender_message;
    ArrayList<String> receiver_message;
    ArrayList<String> message_type;
    ArrayList<File>    re_file;
    ArrayList<String>  re_file_type;

    String APP_STATE = "";
    String MESSAGE_TYPE_NOTIF = "";
    String MESSAGE_TYPE_NOTIF_MESSAGE = "";

    SharedPreferences sharedPreferences;
    public static Handler handler = new Handler();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            builder.detectFileUriExposure();
        }
        bt = new BluetoothSPP(this);

        bt.setBluetoothConnectionListener(this);
        bt.setBluetoothStateListener(this);
        bt.setOnDataReceivedListener(this);
        sharedPreferences = getSharedPreferences(Constants.APP_KEY,MODE_PRIVATE);
        message = (EditText)findViewById(R.id.chat_mesage);
        connect_text = (TextView) findViewById(R.id.connect_text);
        message_history = (TextView)findViewById(R.id.message_container);
        chat_relative = (RelativeLayout)findViewById(R.id.chat_layout);
        home_layout = (RelativeLayout)findViewById(R.id.home_layout);
        fromtop = AnimationUtils.loadAnimation(this,R.anim.fromtop);
        totop = AnimationUtils.loadAnimation(this,R.anim.totop);
        pd = new ProgressDialog(MainActivity.this);

        chat_recycler = (RecyclerView)findViewById(R.id.chat_view);
        chat_recycler.setLayoutManager(new LinearLayoutManager(this));

        sender_message = new ArrayList<>();
        receiver_message = new ArrayList<>();
        message_type = new ArrayList<>();
        re_file      = new ArrayList<>();
        re_file_type = new ArrayList<>();

        chatContainer = new ChatContainer();
        chatContainer.setSender_message(sender_message);
        chatContainer.setReceiver_message(receiver_message);
        chatContainer.setMessage_type(message_type);
        chatContainer.setRe_file(re_file);
        chatContainer.setRe_file_type(re_file_type);

        chatRecycler = new ChatRecycler(MainActivity.this,chatContainer);
        chat_recycler.setAdapter(chatRecycler);


        toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setLogo(R.mipmap.ic_bluetooth);
        toolbar.setTitle(R.string.app_name);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);

    }


    @Override
    protected void onStart() {
        super.onStart();
        if (!bt.isBluetoothEnabled()) {
            // Do somthing if bluetooth is disable
            bt.enable();
            new Common().Toasted(getApplicationContext(),Constants.ENABLING_BLUETOOTH,Constants.INFO);
            //Toast.makeText(this, "BLUETOOTH DISABLED", Toast.LENGTH_SHORT).show();
        } else {
            // Do something if bluetooth is already enable


            if(!bt.isServiceAvailable()) {
                bt.setupService();
                bt.startService(BluetoothState.DEVICE_OTHER); // CAN ADD ANDROID
                setup();

                //Toast.makeText(this, "BLUETOOTH ENABLED", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        APP_STATE = Constants.APP_MINIMIZED;
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        APP_STATE = Constants.APP_ON;
        ViewChatWindow();

    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    public void Settings(View view)
    {
        startActivity(new Intent(getApplicationContext(),Settings.class));
    }
    public void FilePicker(View view) {
        Intent intent = new Intent();
        intent.setType("*/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "SELECT FILE"), PICK_FILE_REQUEST);
    }
    public void FileSender() {

        if (bytes.equals("")) {
            Toast.makeText(this, "Please Select a File", Toast.LENGTH_SHORT).show();
        } else
        {
            pd.setMessage("SENDING FILE "+fileName+"."+extension);
            pd.setCancelable(false);
            pd.show();
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    String base64 = Base64.encodeToString(bytes, Base64.DEFAULT);
                    // Log.d("BYTES-64",base64);
                    String blue_message = "{\"message_type\":\"file\",\"basesf\":\""+base64+"\",\"filename\":\""+fileName+"\",\"fileext\":\""+extension+"\"}";
                    bt.send(blue_message,true);

                }
            };
            AsyncTask.execute(runnable);


        }

    }
    public void ExitApp(View view)
    {

        final AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(MainActivity.this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(MainActivity.this);
        }
        builder.setTitle(Constants.EXIT_APPLICATION)
                .setMessage(Constants.EXIT_APPLICATION_MESSAGE)
                .setPositiveButton(Constants.YES, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (bt.isBluetoothEnabled()) {

                            bt.OffBluetooth();
                        }
                        new Common().Toasted(getApplicationContext(),"Developed By ImDroidGuy",Constants.WARN);
                        Thread background = new Thread() {
                            public void run() {

                                try {
                                    // Thread will sleep for 10 seconds
                                     sleep(2*1000);
                                    System.exit(0);


                                } catch (Exception e) {

                                }
                            }
                        };
                        background.start();


                    }
                })
                .setNegativeButton(Constants.CANCEL, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing

                    }
                })
                .setIcon(R.drawable.power)
                .show();
    }




    public void setup() { }

    public void SendMessage(View view)
    {
        String message_ = message.getText().toString();
        if(message_.isEmpty())
        {
            message.setError("Enter Message");
        }else
        {

            String blue_message = "{\"message_type\":\"message\",\"message\":\""+message_+"\"}";
            bt.send(blue_message, true);
            sender_message.add(message_);
            receiver_message.add("");
            re_file.add(file);
            re_file_type.add("");
            message_type.add(Constants.M_SENDER);
            chat_recycler.smoothScrollToPosition(message_type.size());
            chatRecycler.notifyDataSetChanged();

            message.setText("");

            Notification(Constants.MESSAGE_SEND);
        }

    }
    public void Chat(View view)
    {
        if(BLUE_TOOTH_STATE == BluetoothState.STATE_CONNECTED)
        {
            home_layout.setVisibility(View.INVISIBLE);
            chat_relative.setVisibility(View.VISIBLE);
            chat_relative.startAnimation(fromtop);
        }else
        {
            new Common().Toasted(getApplicationContext(),Constants.CONNECT_DEVICE_INFO,Constants.INFO);
        }

    }

    @Override
    public void onBackPressed() {
        if(chat_relative.getVisibility() == View.VISIBLE)
        {
            chat_relative.setVisibility(View.GONE);
            home_layout.setVisibility(View.VISIBLE);
            chat_relative.startAnimation(totop);
        }
    }

    public void DeviceList(View view) {
        if(bt.isBluetoothEnabled() && BLUE_TOOTH_STATE == BluetoothState.STATE_CONNECTED)
        {
            bt.disconnect();
        }else
        {
            Intent intent = new Intent(getApplicationContext(), DeviceList.class);
            startActivityForResult(intent, BluetoothState.REQUEST_CONNECT_DEVICE);
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        bt.stopService();
    }
    public void onActivityResult(int requestCode, int resultCode,final Intent data) {
        if(requestCode == BluetoothState.REQUEST_CONNECT_DEVICE) {
            if(resultCode == Activity.RESULT_OK)
            {
               // bt.connect(data);
               runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        bt.connect(data);


                    }
                });
            }
//                bt.connect(data);

        } else if(requestCode == BluetoothState.REQUEST_ENABLE_BT) {
            if(resultCode == Activity.RESULT_OK) {
                bt.setupService();
            } else {
                Toast.makeText(getApplicationContext()
                        , "Bluetooth was not enabled."
                        , Toast.LENGTH_SHORT).show();
                finish();
            }
        }else if(requestCode == PICK_FILE_REQUEST  && resultCode == RESULT_OK && data != null && data.getData() != null)
        {
            Uri filePath = data.getData();
            try {
                File file = new File(SelectedFilePath.getPath(getApplicationContext(),filePath));
                fileName = file.getName();
                extension = fileName.substring(fileName.lastIndexOf("."));
                bytes = loadFile(file);
                Log.d("LOADED_BYTES",bytes.length+"");

                sender_message.add("");
                receiver_message.add("");
                re_file.add(file);
                re_file_type.add(extension);
                message_type.add(Constants.M_SENDER_FILE);
                chat_recycler.smoothScrollToPosition(message_type.size());
                chatRecycler.notifyDataSetChanged();

                FileSender();
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(),"ERROR "+e.getMessage()+"\n"+e.getCause(),Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }
    }




    @Override
    public void onDeviceConnected(String name, String address) {
       // Toast.makeText(this, ""+name, Toast.LENGTH_SHORT).show();
        BLUE_TOOTH_STATE = BluetoothState.STATE_CONNECTED;
        new Common().Toasted(getApplicationContext(),name+" "+Constants.DEVICE_CONNECTED,Constants.SUCCESS);
        ViewChatWindow();
    }

    @Override
    public void onDeviceDisconnected() {
       // Toast.makeText(this, "DISCONNECTED", Toast.LENGTH_SHORT).show();
        BLUE_TOOTH_STATE = BluetoothState.STATE_NULL;
        new Common().Toasted(getApplicationContext(),Constants.DEVICE_DISCONNECTED,Constants.INFO);
        connect_text.setText(Constants.CONNECT_DEVICE);
    }

    @Override
    public void onDeviceConnectionFailed() {
        BLUE_TOOTH_STATE = BluetoothState.STATE_NULL;
        new Common().Toasted(getApplicationContext(),Constants.CONNECTION_FAILED,Constants.ERROR);
    }

    @Override
    public void onServiceStateChanged(int state) {

        if(state == BluetoothState.STATE_CONNECTED)
        {

            new Common().Toasted(getApplicationContext(),Constants.DEVICE_CONNECTED,Constants.INFO);
            connect_text.setText(Constants.DISCONNECT_DEVICE);
        }


        else if(state == BluetoothState.STATE_CONNECTING) new Common().Toasted(getApplicationContext(),Constants.CONNECTING_DEVICE,Constants.INFO);
           // Toast.makeText(this, "STATE CHANGED Connecting", Toast.LENGTH_SHORT).show();
        else if(state == BluetoothState.STATE_LISTEN){}
          //  Toast.makeText(this, "STATE CHANGED Listen", Toast.LENGTH_SHORT).show();
        else if(state == BluetoothState.STATE_NONE) {}
          //  Toast.makeText(this, "STATE CHANGED None", Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onDataReceived(byte[] data, String message) {

        try{
            JSONObject jblue_json = new JSONObject(message);
            if(jblue_json.getString("message_type").equals("message"))
            {
                MESSAGE_TYPE_NOTIF = "message";
                String bmessage = jblue_json.getString("message");
                MESSAGE_TYPE_NOTIF_MESSAGE = bmessage;
                sender_message.add("");
                re_file.add(file);
                re_file_type.add("");
                receiver_message.add(bmessage);
                message_type.add(Constants.M_RECIEVER);
                chat_recycler.smoothScrollToPosition(message_type.size());
                chatRecycler.notifyDataSetChanged();
                Notification(Constants.MESSAGE_RECIEVED);
               // message_history.append(bmessage+"\n");
            }else if(jblue_json.getString("message_type").equals("progress")){
                if(pd.isShowing())
                {
                    pd.cancel();
                    Notification(Constants.MESSAGE_SEND);
                }
            }else
            {
                MESSAGE_TYPE_NOTIF = "file";
                String filename = jblue_json.getString("filename");
                String extension = jblue_json.getString("fileext");
                String base64 = jblue_json.getString("basesf");
                byte[] bytesfile = Base64.decode(base64, Base64.DEFAULT);
                FileWriter(bytesfile,filename,extension);
            }
        }catch (Exception e){Log.d("ERROR",e.toString()); }

        //byte[] bytesfile = Base64.decode(message, Base64.DEFAULT);
        //  FileWriter(bytesfile);
        //  Log.d("BYTES-64",message);

    }

    public void FileWriter(byte[] data,String fileName,String extension)
    {
        try{
            File file = new File(Environment.getExternalStorageDirectory(),"/Bluechatz/");
            if(file.exists())
            {

            }
            else
            {
                file.mkdir();
            }
            String file_name = fileName+"."+extension;
            File outputFile = new File(file,file_name);

            FileOutputStream os =  new FileOutputStream(outputFile,true); // remove true to disable append mode
            os.write(data);
            os.flush();
            os.close();

            String blue_message = "{\"message_type\":\"progress\"}";
            bt.send(blue_message, true);

            sender_message.add("");
            receiver_message.add("");
            re_file.add(outputFile);
            re_file_type.add(extension);
            message_type.add(Constants.M_RECIEVER_FILE);
            chat_recycler.smoothScrollToPosition(message_type.size());
            chatRecycler.notifyDataSetChanged();

        }catch (Exception e)
        {
            Log.d("BYTES_TRAN_ERROR_2",e.toString());
        }
        finally {
              Notification(Constants.MESSAGE_RECIEVED);
           // String blue_message = "{\"message_type\":\"progress\"}";
           // bt.send(blue_message, true);
        }
    }
    private static byte[] loadFile(File file) throws IOException {
        InputStream is = new FileInputStream(file);

        long length = file.length();
        if (length > Integer.MAX_VALUE) {
            // File is too large
        }
        byte[] bytes = new byte[(int)length];

        int offset = 0;
        int numRead = 0;
        while (offset < bytes.length
                && (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
            offset += numRead;
        }

        if (offset < bytes.length) {
            throw new IOException("Could not completely read file "+file.getName());
        }

        is.close();
        return bytes;
    }
    public void FileOpen(int position)
    {
        MimeTypeMap myMime = MimeTypeMap.getSingleton();
        Intent newIntent = new Intent(Intent.ACTION_VIEW);
        String mimeType = myMime.getMimeTypeFromExtension(fileExt(re_file.get(position).getPath()).substring(1));
        newIntent.setDataAndType(Uri.fromFile(re_file.get(position)),mimeType);
        newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        try {
              startActivity(newIntent);
        } catch (ActivityNotFoundException e) {
            new Common().Toasted(getApplicationContext(),Constants.CANNOT_OPEN_FILE,Constants.ERROR);
           // Toast.makeText(context, "No handler for this type of file.", Toast.LENGTH_LONG).show();
        }
    }
    public void ImageOpen(int position)
    {
        Intent image_view = new Intent(getApplicationContext(),ImageViews.class);
        image_view.putExtra("file_path",re_file.get(position).toString());
        startActivity(image_view);
    }
    private String fileExt(String url) {
        if (url.indexOf("?") > -1) {
            url = url.substring(0, url.indexOf("?"));
        }
        if (url.lastIndexOf(".") == -1) {
            return null;
        } else {
            String ext = url.substring(url.lastIndexOf(".") + 1);
            if (ext.indexOf("%") > -1) {
                ext = ext.substring(0, ext.indexOf("%"));
            }
            if (ext.indexOf("/") > -1) {
                ext = ext.substring(0, ext.indexOf("/"));
            }
            return ext.toLowerCase();

        }
    }
    public void ViewChatWindow()
    {
        if(BLUE_TOOTH_STATE == BluetoothState.STATE_CONNECTED)
        {
            home_layout.setVisibility(View.INVISIBLE);
            chat_relative.setVisibility(View.VISIBLE);
            //  chat_relative.startAnimation(fromtop);
        }else
        {
            new Common().Toasted(getApplicationContext(),Constants.CONNECT_DEVICE_INFO,Constants.INFO);
        }
    }
    public void Notification(String constants)
    {
        String settings_mute = sharedPreferences.getString("settings_mute","");
        if(constants.equals(Constants.MESSAGE_SEND))
        {
            try {
                Uri notification = Uri.parse("android.resource://guy.droid.im.bluetooth/"+R.raw.tick);
                //   Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
                if(settings_mute.equals("true"))
                {

                }else
                {
                    r.play();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if(constants.equals(Constants.MESSAGE_RECIEVED) && (APP_STATE.equals(Constants.APP_ON)) || APP_STATE.equals(""))
        {
            try {
                Uri notification = Uri.parse("android.resource://guy.droid.im.bluetooth/"+R.raw.softtick);
                //   Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
               // r.play();
                if(settings_mute.equals("true"))
                {

                }else
                {
                    r.play();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if(constants.equals(Constants.MESSAGE_RECIEVED) && APP_STATE.equals(Constants.APP_MINIMIZED))
        {

            Intent actionIntent = new Intent(getApplicationContext(), MainActivity.class);

            PendingIntent actionPendingIntent =
                    PendingIntent.getActivity(getApplicationContext(), 0, actionIntent,
                            PendingIntent.FLAG_UPDATE_CURRENT);
            String MESSAGE_TITLE = "NEW MESSAGE RECIEVED ";
            if(MESSAGE_TYPE_NOTIF.equals("message"))
            {
                MESSAGE_TITLE = "NEW MESSAGE RECIEVED ";
            }else
            {
                MESSAGE_TYPE_NOTIF_MESSAGE = "";
                MESSAGE_TITLE = "NEW FILE RECIEVED ";
            }

            NotificationCompat.Action action =
                    new NotificationCompat.Action.Builder(R.drawable.bluetooth,
                            MESSAGE_TITLE, actionPendingIntent)
                            .build(); // Action for Notification in Status Bar
            NotificationManagerCompat mgr = NotificationManagerCompat.from(getApplicationContext());

            NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext())
                    .setContentTitle(MESSAGE_TITLE)
                    .setContentText(MESSAGE_TYPE_NOTIF_MESSAGE).setAutoCancel(true).setContentIntent(actionPendingIntent)
                    .setSmallIcon(R.drawable.ic_notifme).addAction(action);

            mgr.notify(Constants.NOTIFICATION_ID,builder.build());
            try {
               // Uri notification = Uri.parse("android.resource://guy.droid.im.bluetooth/"+R.raw.softtick);
                Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);

              //  r.play();
                if(settings_mute.equals("true"))
                {

                }else
                {
                    r.play();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
}