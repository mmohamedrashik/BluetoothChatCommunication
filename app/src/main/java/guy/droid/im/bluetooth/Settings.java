package guy.droid.im.bluetooth;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

public class Settings extends AppCompatActivity {

    Switch muteswitch;
    Toolbar toolbar;
    SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        muteswitch = (Switch)  findViewById(R.id.mute);
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setLogo(R.mipmap.ic_bluetooth);
        toolbar.setTitle(R.string.app_name);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        sharedPreferences = getSharedPreferences(Constants.APP_KEY,MODE_PRIVATE);

        String settings_mute = sharedPreferences.getString("settings_mute","");
        if(settings_mute.equals("") || settings_mute.equals("false"))
        {
            muteswitch.setChecked(false);
        }else
        {
            muteswitch.setChecked(true);
        }
        muteswitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked == true)
                {
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("settings_mute","true");
                    editor.commit();
                }else
                {
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("settings_mute","false");
                    editor.commit();
                }
            }

        });


    }
    public void launchMarket(View view)
    {
        Uri uri = Uri.parse("market://details?id=" + this.getPackageName());
        Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
        try
        {
            startActivity(myAppLinkToMarket);
        }
        catch (ActivityNotFoundException e)
        {
            Toast.makeText(this, " Sorry, Not able to open!", Toast.LENGTH_SHORT).show();
        }
    }

    public void ShareApp(View view)
    {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=guy.droid.im.bluetooth");
        startActivity(Intent.createChooser(intent, "Share"));
    }

}
